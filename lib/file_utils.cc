#include "lib/file_utils.hpp"

patrick::Unit available_space_human_readable(const std::filesystem::path& analyze_path) {
    return patrick::Unit(0, "B");
}

patrick::Unit available_space_in_ibibytes(const std::filesystem::path& analyze_path) {
    return patrick::Unit();
}