#ifndef LIB_UNIT_H
#define LIB_UNIT_H

#include <string>
#include <sstream>

namespace patrick {
    class Unit {
        private:
            int value = 0;
            std::string measure = "";
        public:
            Unit() {}
            Unit(int value, std::string measure) {
                this->value=value;
                this->measure = measure;
            }

            void SetValue(int value) {
                this->value = value;
            }

            void SetMeasure(std::string measure) {
                this->measure = measure;
            }

            int GetValue() {
                return this->value;
            }

            std::string GetMeasure() {
                return this->measure;
            }

            std::string ToString() {
                std::stringstream stream;
                stream << this->GetValue() << this->GetMeasure();
                return stream.str();
            }
    };
}

#endif