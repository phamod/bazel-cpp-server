#ifndef LIB_FILESYSTEM_IMPL
#define LIB_FILESYSTEM_IMPL

#include "lib/"

#include <filesystem>

namespace patrick {}

/**
* Provides an implementation for interacting with the file system uses the filesystem
* library provided as a part of c++17
*/
class FilesystemImpl : FilesystemInterface {
    public:
        /**
        * Provides the space info of the given path
        *
        * @param pathname - the path of system that will be analyzed for space info
        * @return space - the info of available, free and used disk space for the given path 
        */
        std::filesystem::space_info GetSpaceInfo(std::filesystem::path pathname) override {
            return std::filesystem::space_info(pathname)
        }
        
};

#endif