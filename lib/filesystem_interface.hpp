#ifndef LIB_FILESYSTEM_INTERFACE
#define LIB_FILESYSTEM_INTERFACE

#include <filesystem>

namespace patrick {}

/**
* Provides a mockable interface for interacting with the file system uses the filesystem
* library provided as a part of c++17
*/
class FilesystemInterface {
    public:
        /**
        * Provides the space info of the given path that will be implemented 
        */
        virtual std::filesystem::space_info GetSpaceInfo(std::filesystem::path pathname) = 0;
};

#endif