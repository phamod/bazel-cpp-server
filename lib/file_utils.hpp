#ifndef LIB_FILE_UTILS_H
#define LIB_FILE_UTILS_H

#include "lib/unit.hpp"
#include <filesystem>

patrick::Unit available_space_human_readable(const std::filesystem::path& analyze_path);
patrick::Unit available_space_in_ibibytes(const std::filesystem::path& analyze_path); 



#endif