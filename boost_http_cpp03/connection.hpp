//
// connection.hpp
// ~~~~~~~~~~~~~~
//
// Copyright (c) 2003-2021 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef HTTP_CONNECTION_HPP
#define HTTP_CONNECTION_HPP

#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/array.hpp>
#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include "reply.hpp"
#include "request.hpp"
#include "request_handler.hpp"
#include "request_parser.hpp"

namespace http {
namespace server {

typedef boost::asio::ssl::stream<boost::asio::ip::tcp::socket> ssl_socket;

class connection_manager;

/// Represents a single connection from a client.
class connection
  : public boost::enable_shared_from_this<connection>
    // ,private boost::noncopyable
{
public:
  /// Construct a connection with the given io_context.
  // connection(boost::asio::io_context& io_context,
  //     connection_manager& manager, request_handler& handler);

  // construct for an ssl/tls connection
  explicit connection(boost::asio::io_context& io_context,
    boost::asio::ssl::context& context,
      connection_manager& manager, 
      request_handler& handler);

  /// Get the socket associated with the connection.
  ssl_socket::lowest_layer_type& socket();

  /// Start the first asynchronous operation for the connection.
  void start();

  /// Stop all asynchronous operations associated with the connection.
  void stop();

private:

  // handle ssl/tls handshake
  void handle_handshake(const boost::system::error_code& error);

  /// Handle completion of a read operation.
  void handle_read(const boost::system::error_code& e,
      std::size_t bytes_transferred);

  /// Handle completion of a write operation.
  void handle_write(const boost::system::error_code& e);

  /// Socket for the connection.
  // boost::asio::ip::tcp::socket socket_;

  /// Attempt an SSL/TLS conntection
  ssl_socket ssl_socket_;
  // boost::asio::ssl::context context_;

  /// The manager for this connection.
  connection_manager& connection_manager_;

  /// The handler used to process the incoming request.
  request_handler& request_handler_;

  /// Buffer for incoming data.
  boost::array<char, 8192> buffer_;

  /// The incoming request.
  request request_;

  /// The parser for the incoming request.
  request_parser request_parser_;

  /// The reply to be sent back to the client.
  reply reply_;

  enum { max_length = 1024 };
  char data_[max_length];
};

typedef boost::shared_ptr<connection> connection_ptr;

} // namespace server
} // namespace http

#endif // HTTP_CONNECTION_HPP
