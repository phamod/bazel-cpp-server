FROM fedora

RUN dnf install --nodocs -y bzip2 gcc-c++
RUN dnf clean all

COPY boost_1_78_0.tar.bz2 /
RUN tar xf boost_1_78_0.tar.bz2
WORKDIR /boost_1_78_0
RUN ./bootstrap.sh
RUN ./b2