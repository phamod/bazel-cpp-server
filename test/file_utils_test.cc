#include "lib/file_utils.hpp"
#include <gtest/gtest.h>
#include <filesystem>
#include <iostream>

TEST(FileUtilsTest, TestSpaceAvailableHumanReadable) {
    std::filesystem::path file = "./";
    std::string unit = "B";
    patrick::Unit space = available_space_human_readable(file);
    std::cout << space.ToString() << std::endl;
    EXPECT_NE(space.GetValue(), -1);
}

TEST(UnitTest, TestToStringFromDefaultConstructor) {
    patrick::Unit unit = patrick::Unit();
    EXPECT_EQ(unit.ToString(),"0");   
}

TEST(UnitTest, TestToStringFromConstructor) {
    patrick::Unit unit = patrick::Unit(0,"B");
    EXPECT_EQ(unit.ToString(), "0B");
}

TEST(UnitTest, TestToStringUsingSetters) {
    patrick::Unit unit;
    unit.SetMeasure("B");
    unit.SetValue(100);
    EXPECT_EQ(unit.ToString(), "100B");
}