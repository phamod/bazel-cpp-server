# bazel cpp server
Project to create a simple client server interface using the boost library.

## software versions
- OS fedora 35
- docker 20.10.12
- gcc/g++ 11.2.1
- openjdk 11.0.13
- bazel 4.2.2
- boost 1.78.0 (built from scratch)
- python 3.10.1